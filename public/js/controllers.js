function invoicesCtrl($scope, Invoices, InvoiceItems, Customers, Products) {
    $scope.invoices = []
    Invoices.query(function (res) {
        angular.forEach(res, function (invoice) {
            $scope.invoices.push({
                id: invoice.id,
                customer: Customers.get({ customerId: invoice.customer_id }),
                discount: invoice.discount,
                total: invoice.total,
                items: $scope.products(invoice.id)
            })
        })
    })

    $scope.products = function (invoiceId) {
        let items = []
        InvoiceItems.query({ invoiceId: invoiceId }, function (res) {
            angular.forEach(res, function (product) {
                items.push({
                    product: Products.get({ productId: product.product_id }),
                    quantity: product.quantity
                })
            })
        })
        return items
    }

    $scope.delete = function (index, id) {
        Invoices.delete({ invoiceId: id }, function () {
            $scope.invoices.splice(index, 1)
        })
    }
}

function productsCtrl($scope, Products) {
    $scope.productsList = Products.query()
}

function customersCtrl($scope, Customers) {
    $scope.customersList = Customers.query()
}

function invoiceCtrl($scope, Invoices, InvoiceItems, Customers, Products) {
    $scope.customersList = Customers.query()
    $scope.productsList = Products.query()
    $scope.invoice = {
        customer: '',
        discount: 0,
        items: []
    }

    $scope.parseJson = function (json) {
        return angular.fromJson(json)
    }

    $scope.add = function () {
        $scope.invoice.items.push({
            product: {
                price: 0
            },
            quantity: 1
        })
    }

    $scope.remove = function (index) {
        $scope.invoice.items.splice(index, 1)
    }

    $scope.subTotalPrice = function () {
        let subTotal = 0.00
        angular.forEach($scope.invoice.items, function (item) {
            subTotal += item.quantity * $scope.parseJson(item.product).price
        })
        return subTotal
    }

    $scope.discountPrice = function () {
        return (($scope.invoice.discount * $scope.subTotalPrice()) / 100)
    }

    $scope.totalPrice = function () {
        $scope.invoice.total = $scope.subTotalPrice() - $scope.discountPrice()
        return $scope.invoice.total
    }

    $scope.price = function (item) {
        const product = JSON.parse(item)
        $scope.productPrice = product.price
    }

    $scope.save = function () {
        Invoices.save({
            customer_id: $scope.invoice.customer,
            discount: $scope.invoice.discount,
            total: $scope.invoice.total
        }, function (resp) {
            angular.forEach($scope.invoice.items, function (item) {
                InvoiceItems.save({ invoiceId: resp.id }, { product_id: $scope.parseJson(item.product).id, quantity: item.quantity })
            })
        })
    }
}

angular
    .module('invoicesApp')
    .controller('InvoicesCtrl', invoicesCtrl)
    .controller('ProductsCtrl', productsCtrl)
    .controller('CustomersCtrl', customersCtrl)
    .controller('InvoiceCtrl', invoiceCtrl)