function invoices($resource) {
    return $resource('/api/invoices/:invoiceId', null)
}

function invoiceItems($resource) {
    return $resource('/api/invoices/:invoiceId/items/:itemsId', null)
}

function products($resource) {
    return $resource('/api/products/:productId', null)
}

function customers($resource) {
    return $resource('/api/customers/:customerId', null)
}

angular
    .module('invoicesApp')
    .factory('Invoices', invoices)
    .factory('InvoiceItems', invoiceItems)
    .factory('Products', products)
    .factory('Customers', customers)