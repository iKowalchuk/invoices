function config($routeProvider, $locationProvider) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    })

    $routeProvider
        .when("/invoices", {
            templateUrl: "views/invoices.html"
        })
        .when("/products", {
            templateUrl: "views/products.html"
        })
        .when("/customers", {
            templateUrl: "views/сustomers.html"
        })
        .when("/invoice", {
            templateUrl: "views/invoice.html"
        })
        .otherwise("/invoices")
}
angular
    .module('invoicesApp')
    .config(config)